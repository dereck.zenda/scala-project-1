package part1basics

object ValueVariableTypes extends App {
  val x: Int = 42

  val z = 42

  println(x)
  println(z)

  val aString: String = "hello"
  val bString ="Goodbye"

  val aBoolean: Boolean = false
  val aChar: Char = 'a'
  val aShort: Short = 4613
  val aLong: Long = 4545454545455555555L
  val aDouble: Double = 3.14

  // val immutable
  // variables mutable

  var aVariable: Int = 4
  aVariable =9 // side effects


}
