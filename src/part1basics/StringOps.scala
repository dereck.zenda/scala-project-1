package part1basics

/**
  * Author: Dereck Zenda
  * Date: 03/01/2019 3:44 PM
  * Project: scala-project-1
  * Package: part1basics
  */

object StringOps extends App {
  val str: String = "Hello, I'm learning Scala"

  println(str.charAt(2))
  println(str.substring(7,11))
  println(str.split( " ").toList)
  println(str.replace(" ","-"))
  println(str.toUpperCase())
  println(str.length)
  println(str.toLowerCase())

  val name= "Dereck"
  val age = 12
  val greeting= s"Hello, my nam is $name and my age is $age, and will ${age +1 } next year"
  println(greeting)

  val speed =1.2f
  val myth=f"$name can eat $speed%2.2f chocs per minute"
  println(myth)

  println(raw" This is a \n newline")
  val escaped = "This is a \n newline"
  println(raw"$escaped")


}
