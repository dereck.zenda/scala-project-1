package part1basics

object Functions extends App {

  def aFunction(a: String, b: Int) =
    a + " " + b

  println (aFunction("hello",3))

  def aPlessFunction(): Int=42

  def aRepeatedFunction(aString: String, n:Int):String={
    if (n==1) aString
    else aString + aRepeatedFunction(aString, n-1)
  }

  println(aRepeatedFunction("hello",3))

  def aGreetingFunction(name: String, age: Int): String={
    "My name is " + name+ " and my age is " + age
  }

  println(aGreetingFunction("Dereck",30))

  def aFactorial(n:Int):Int={
    if(n<=0)1
    else n+aFactorial(n-1)
  }

  println(aFactorial(5))


  def fibonacci(n: Int): Int={
    if(n<=1) 1
    else fibonacci(n-1) + fibonacci(n-2)
  }

  println(fibonacci(8))

}
