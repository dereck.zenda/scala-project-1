package part1basics

import java.util.concurrent.atomic.DoubleAccumulator

object Recursion extends App {

  def aFactorial(n:Int):Int={
    if(n<=0)1
    else {
      println( "printing factorial of " + n + " first need factorial of " + (n-1))
      val result= n * aFactorial(n-1)
      println( "Computed factorial of " + n)
      result
    }
  }

  println(aFactorial(5))

  def calledByValue(x: Long):Unit={
    println( "by value " +x)
    println("by Value " +x)
  }

  def calledByName(x: => Long):Unit={
    println( "by name " +x)
    println("by name " +x)
  }

  calledByValue(System.nanoTime())
  calledByName(System.nanoTime())



}









